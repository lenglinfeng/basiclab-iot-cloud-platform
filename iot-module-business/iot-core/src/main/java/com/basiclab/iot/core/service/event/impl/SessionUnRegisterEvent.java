package com.basiclab.iot.core.service.event.impl;

import com.basiclab.iot.core.executor.event.EventParam;
import com.basiclab.iot.core.executor.event.SingleEvent;
import com.basiclab.iot.core.service.event.SingleEventConstants;

/**
 * Created by jiangwenping on 2017/5/22.
 * 网络链接断开
 */
public class SessionUnRegisterEvent extends SingleEvent<Long> {
    public SessionUnRegisterEvent(Long eventId, long shardingId, EventParam... parms) {
        super(SingleEventConstants.sessionUnRegister, eventId, shardingId, parms);
    }
}
