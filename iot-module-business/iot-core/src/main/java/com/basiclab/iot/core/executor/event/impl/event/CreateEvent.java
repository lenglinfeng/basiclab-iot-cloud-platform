package com.basiclab.iot.core.executor.event.impl.event;

import com.basiclab.iot.core.executor.common.utils.Loggers;
import com.basiclab.iot.core.executor.event.CycleEvent;
import com.basiclab.iot.core.executor.event.EventParam;
import com.basiclab.iot.core.executor.event.EventType;

import java.io.Serializable;

/**
 * Created by jiangwenping on 17/1/11.
 *  dispatch thread使用
 */
public class CreateEvent<ID extends Serializable> extends CycleEvent {

    public CreateEvent(EventType eventType, ID eventId, EventParam... parms){
//        setEventType(eventType);
//        setParams(parms);
        super(eventType, eventId, parms);
    }

    @Override
    public void call() {
        if(Loggers.gameExecutorUtil.isDebugEnabled()){
            EventParam[] eventParams = getParams();
            Loggers.gameExecutorUtil.debug("create event " + eventParams[0].getT());
        }

    }
}
