package com.basiclab.iot.core.message.logic.tcp.common;

import com.basic.common.network.annotation.MessageCommandAnnotation;
import com.basic.common.network.exception.CodecException;
import com.basiclab.iot.core.message.auto.common.CommonMessageProBuf;
import com.basiclab.iot.core.service.message.AbstractNetProtoBufTcpMessage;
import com.basiclab.iot.core.service.message.command.MessageCommandIndex;

/**
 * Created by jiangwenping on 17/2/20.
 */
@MessageCommandAnnotation(command = MessageCommandIndex.COMMON_RESPONSE_MESSAGE)
public class CommonResponseServerMessage extends AbstractNetProtoBufTcpMessage {

    @Override
    public void decoderNetProtoBufMessageBody() throws Exception {
        byte[] bytes = getNetMessageBody().getBytes();
        CommonMessageProBuf.CommonResponseServerProBuf req = CommonMessageProBuf.CommonResponseServerProBuf.parseFrom(bytes);
    }

    @Override
    public void release() throws CodecException {

    }

    @Override
    public void encodeNetProtoBufMessageBody() throws Exception {
        CommonMessageProBuf.CommonResponseServerProBuf.Builder builder = CommonMessageProBuf.CommonResponseServerProBuf.newBuilder();
        byte[] bytes = builder.build().toByteArray();
        getNetMessageBody().setBytes(bytes);
    }

}
