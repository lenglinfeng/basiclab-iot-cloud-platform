package com.basiclab.iot.core.executor.update.pool;

import com.basic.common.network.util.ExecutorUtil;
import com.basiclab.iot.core.executor.common.utils.Constants;
import com.basiclab.iot.core.executor.update.entity.IUpdate;
import com.basiclab.iot.core.executor.update.thread.dispatch.DispatchThread;
import com.basiclab.iot.core.executor.update.thread.listener.LockSupportUpdateFutureListener;
import com.basiclab.iot.core.executor.update.thread.update.LockSupportUpdateFuture;
import com.basiclab.iot.core.executor.update.thread.update.LockSupportUpdateFutureThread;
import com.basiclab.iot.core.thread.executor.NonOrderedQueuePoolExecutor;
import com.basiclab.iot.core.thread.factory.GameThreadPoolHelpFactory;
import com.basiclab.iot.core.thread.policy.RejectedPolicyType;

import java.util.concurrent.TimeUnit;

/**
 * Created by jiangwenping on 17/1/11.
 * 更新执行器
 */
public class UpdateExecutorService implements IUpdateExecutor {

    private final NonOrderedQueuePoolExecutor nonOrderedQueuePoolExecutor;


    public UpdateExecutorService(int corePoolSize, int maxSize, RejectedPolicyType rejectedPolicyType) {
        String name = Constants.Thread.UpdateExecutorService;
        GameThreadPoolHelpFactory gameThreadPoolHelpFactory = new GameThreadPoolHelpFactory();
        nonOrderedQueuePoolExecutor = new NonOrderedQueuePoolExecutor(Constants.Thread.UpdateExecutorService, corePoolSize, maxSize, gameThreadPoolHelpFactory.createPolicy(rejectedPolicyType, name));
    }

    @Override
    public void executorUpdate(DispatchThread dispatchThread, IUpdate iUpdate, boolean firstFlag, int updateExcutorIndex) {
        LockSupportUpdateFuture lockSupportUpdateFuture = new LockSupportUpdateFuture(dispatchThread);
        lockSupportUpdateFuture.addListener(new LockSupportUpdateFutureListener());
        LockSupportUpdateFutureThread lockSupportUpdateFutureThread = new LockSupportUpdateFutureThread(dispatchThread, iUpdate, lockSupportUpdateFuture);
        nonOrderedQueuePoolExecutor.execute(lockSupportUpdateFutureThread);
    }

    @Override
    public void startup() {

    }

    @Override
    public void shutdown() {
        ExecutorUtil.shutdownAndAwaitTermination(nonOrderedQueuePoolExecutor, 60,
                TimeUnit.MILLISECONDS);
    }
}
