package com.basiclab.iot.core.service.net.broadcast;

import com.basiclab.iot.core.service.message.AbstractNetMessage;

/**
 * Created by jiangwenping on 2017/11/14.
 */
public interface IBroadCastService {
    public void broadcastMessage(long sessionId, AbstractNetMessage NetMessage);
}
