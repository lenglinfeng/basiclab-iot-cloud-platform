package com.basiclab.iot.core.future;

/**
 * Created by jiangwenping on 16/12/27.
 */
public interface ITaskFutureListener<V extends ITaskFuture<?>> extends EventListener {

    /**
     *  完成
     * @param future
     * @throws Exception
     */
    void operationComplete(ITaskFuture<V> future) throws Exception;
}
