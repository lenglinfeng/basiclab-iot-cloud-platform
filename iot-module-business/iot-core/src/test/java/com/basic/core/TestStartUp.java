package com.basic.core;

import com.basic.common.network.util.BeanUtil;
import com.basiclab.iot.core.bootstrap.manager.LocalMananger;
import com.basiclab.iot.core.bootstrap.manager.spring.LocalSpringBeanManager;
import com.basiclab.iot.core.bootstrap.manager.spring.LocalSpringServiceManager;
import com.basiclab.iot.core.bootstrap.manager.spring.LocalSpringServicerAfterManager;
import com.basiclab.iot.core.service.message.registry.MessageRegistry;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by jiangwenping on 17/4/19.
 */
public final class TestStartUp {
    private TestStartUp() {
    }

    public static void startUpWithSpring() throws Exception {
        ClassPathXmlApplicationContext classPathXmlApplicationContext = new ClassPathXmlApplicationContext(new String[]{"bean/*.xml"});
        LocalSpringServiceManager localSpringServiceManager = (LocalSpringServiceManager) BeanUtil.getBean("localSpringServiceManager");
        LocalSpringBeanManager localSpringBeanManager = (LocalSpringBeanManager) BeanUtil.getBean("localSpringBeanManager");
        LocalSpringServicerAfterManager localSpringServicerAfterManager  = (LocalSpringServicerAfterManager) BeanUtil.getBean("localSpringServicerAfterManager");
        LocalMananger.getInstance().setLocalSpringBeanManager(localSpringBeanManager);
        LocalMananger.getInstance().setLocalSpringServiceManager(localSpringServiceManager);
        LocalMananger.getInstance().setLocalSpringServicerAfterManager(localSpringServicerAfterManager);
        try {
            localSpringServiceManager.start();
            localSpringServicerAfterManager.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
        LocalMananger.getInstance().create(MessageRegistry.class, MessageRegistry.class);
        localSpringServiceManager.setMessageRegistry(LocalMananger.getInstance().get(MessageRegistry.class));
    }

//    public static void startUp() throws  Exception{
//        LocalSpringServiceManager localSpringServiceManager = new LocalSpringServiceManager();
//        LocalSpringBeanManager localSpringBeanManager = new LocalSpringBeanManager();
//        MessageCommandFactory messageCommandFactory = new MessageCommandFactory();
//        localSpringBeanManager.setMessageCommandFactory(messageCommandFactory);
//        GameServerConfigService gameServerConfigService = new GameServerConfigService();
//        gameServerConfigService.startup();
//        localSpringServiceManager.setGameServerConfigService(gameServerConfigService);
//        LocalMananger.getInstance().setLocalSpringServiceManager(localSpringServiceManager);
//        LocalMananger.getInstance().setLocalSpringBeanManager(localSpringBeanManager);
//        LocalMananger.getInstance().create(MessageRegistry.class, MessageRegistry.class);
//        localSpringServiceManager.setMessageRegistry(LocalMananger.getInstance().get(MessageRegistry.class));
//    }

}
