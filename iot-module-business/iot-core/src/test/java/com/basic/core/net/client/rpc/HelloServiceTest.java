package com.basic.core.net.client.rpc;


import com.basic.core.TestStartUp;
import com.basiclab.iot.core.service.rpc.client.RpcContextHolder;
import com.basiclab.iot.core.service.rpc.client.RpcContextHolderObject;
import com.basiclab.iot.core.service.rpc.client.RpcProxyService;
import com.basic.common.network.enums.BOEnum;
import com.basic.common.network.util.BeanUtil;
import com.basiclab.iot.core.service.rpc.service.client.HelloService;
import org.junit.Assert;


/**
 * Created by jwp on 2017/3/8.
 */
public class HelloServiceTest {

    private RpcProxyService rpcProxyService;

    public static void main(String[] args) throws Exception {
        HelloServiceTest helloServiceTest = new HelloServiceTest();
        helloServiceTest.init();
        helloServiceTest.helloTest1();
        helloServiceTest.setTear();
    }
    public void init() throws Exception {
        TestStartUp.startUpWithSpring();
        rpcProxyService = (RpcProxyService) BeanUtil.getBean("rpcProxyService");
    }

    public void helloTest1() {
        HelloService helloService = rpcProxyService.createProxy(HelloService.class);
//        HelloService helloService = rpcProxyService.createRemoteProxy(HelloService.class);
        int serverId = 8001;
        RpcContextHolderObject rpcContextHolderObject = new RpcContextHolderObject(BOEnum.WORLD, serverId);
        RpcContextHolder.setContextHolder(rpcContextHolderObject);
        String result = helloService.hello("World");
        System.out.println(result);
        Assert.assertEquals("Hello! World", result);
    }

    public void setTear(){
        if (rpcProxyService != null) {
            try {
                rpcProxyService.shutdown();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
