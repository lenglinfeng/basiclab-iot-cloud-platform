package com.basic.core.common.dict;

import com.basic.core.TestStartUp;
import com.basic.core.service.dict.entity.Bullet;
import com.basic.core.service.dict.entity.RoleSkill;
import com.basiclab.iot.core.bootstrap.manager.LocalMananger;
import com.basiclab.iot.core.service.dict.DictService;

import java.util.List;

/**
 * Created by jiangwenping on 17/5/9.
 */
public final class DictServiceTest {
    public static void main(String[] args) throws Exception {
        TestStartUp.startUpWithSpring();
        DictService dictService = LocalMananger.getInstance().getLocalSpringServiceManager().getDictService();
        String dictModleType= "BULLET_BULLET";
        int id = 1;
        Bullet bullet = dictService.getIDict(dictModleType, id, Bullet.class);
        System.out.println(bullet);
        dictModleType= "STATUS_ROLE_SKILL";
        id = 100;
        List<RoleSkill> roleSkills = dictService.getIDictArray(dictModleType, id, RoleSkill.class);
        System.out.println(roleSkills);
    }
}
