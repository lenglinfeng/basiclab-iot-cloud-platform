package com.basic.core.net.client.proxy;

import com.basic.core.net.client.tcp.GameClient;
import com.basic.core.TestStartUp;
import com.basiclab.iot.core.bootstrap.manager.LocalMananger;
import com.basiclab.iot.core.bootstrap.manager.spring.LocalSpringServiceManager;
import com.basiclab.iot.core.service.message.registry.MessageRegistry;

/**
 * Created by jiangwenping on 2017/6/28.
 */
public class ProxyClient extends GameClient {
    public static void main(String[] args) throws Exception {
        TestStartUp.startUpWithSpring();
        LocalSpringServiceManager localSpringServiceManager = LocalMananger.getInstance().getLocalSpringServiceManager();
        localSpringServiceManager.setMessageRegistry(LocalMananger.getInstance().get(MessageRegistry.class));

        new GameClient().connect("127.0.0.1", 9090);
    }
}
